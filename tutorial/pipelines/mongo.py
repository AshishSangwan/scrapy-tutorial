import pymongo
from ._base import SettingsRequiredPipeline
from itemadapter import ItemAdapter


class MongoPipeline(SettingsRequiredPipeline):
    DEFAULT_MONGODB_HOST = '192.168.11.102'
    DEFAULT_MONGODB_PORT = 27017
    DEFAULT_DB_NAME = 'test'
    DEFAULT_COLLECTION_NAME = 'quotes_items'

    def __init__(self, settings):
        connection = pymongo.MongoClient(
            self.DEFAULT_MONGODB_HOST,
            self.DEFAULT_MONGODB_PORT
        )
        db = connection[self.DEFAULT_DB_NAME]
        self.collection = db[self.DEFAULT_COLLECTION_NAME]
        super(MongoPipeline, self).__init__(settings)

    def process_item(self, item, spider):
        self.collection.insert_one(ItemAdapter(item).asdict())
        return item
