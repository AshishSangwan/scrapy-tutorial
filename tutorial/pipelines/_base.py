import abc


class SettingsRequiredPipeline(object, metaclass=abc.ABCMeta):
    @classmethod
    def from_crawler(cls, crawler):
        return cls(settings=crawler.settings)

    def __init__(self, settings):
        self.settings = settings

    @abc.abstractmethod
    def process_item(self, item, spider):
        pass
